package Filter;

import Fourier.*;

/**
 * Created by Zell on 1/6/16.
 */
public class LowPass {
    public final Complex[] function_fre;
    public Complex[] function_time;
    private final int N;
    private final double cutOff;

    public LowPass(double[] buffer, long frameRate) {
        N = buffer.length;
        cutOff = (double)80/(double)frameRate;
        function_fre = new Complex[N];
        function_time = new Complex[N];
        for(int k = 0; k < N; k++){
            double s0 = (double)k/(double)N;
            double real = (cutOff*cutOff)/(cutOff*cutOff + s0*s0);
            double img = (cutOff*s0)/(s0*s0 + cutOff*cutOff);
            function_fre[k] = new Complex(real, img);
        }

        function_time = FFT.ifft(function_fre);
    }
}
