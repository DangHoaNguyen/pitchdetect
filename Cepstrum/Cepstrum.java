package Cepstrum;

import Fourier.*;

/**
 * Created by Zell on 12/3/15.
 */
public class Cepstrum extends FFT{
    public static double[] Cepstrum(double input[], int samples){
        Complex[] x = new Complex[samples];

        for(int i = 0; i < samples; i++){
            x[i] = new Complex(input[i], 0);
        }

        Complex[] y = fft(x);

        Complex[] c = new Complex[samples];

        int i = 0;
        for(Complex complex: y){
            double abs = complex.abs()*hamming(y.length, i);
            c[i] = new Complex(Math.log(abs), 0);
            i++;
        }

        Complex[] ceptstrum = ifft(c);
        double[] cepstrum_mag = new double[samples];
        for(int n = 0; n < samples; n++){
            cepstrum_mag[n] = ceptstrum[n].abs();
        }

        return cepstrum_mag;
    }

    public double getFrequency(double[] array, int frameRate){

        //initialize the peak as of frequency 80Hz - lower bound of fundamental frequency
        double max = array[frameRate/260];
        double frequency = 0;
        int i = 0;

        //Find the peak in range 80Hz - 260Hz
        for(i = (frameRate/260); i < (frameRate/80); i++){
            if(array[i] > max) {
                max = array[i];
                frequency = frameRate/i;
            }
        }

        //frequency = frame rate / position of peak
        return frequency;
    }

    public static double hamming(int length, int index){
        double angle = (2*Math.PI*index)/((double)length);
        double hamming = 0.54 - 0.46*Math.cos(angle);

        return hamming;
    }

}
